﻿using TechTalk.SpecFlow;

namespace Example
{
    [Binding]
    public class CalculatorSteps
    {
        private int Result { set; get; }
        private Calculator calculator = new Calculator();

        [Given("I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredSomethingIntoTheCalculator(int number)
        {
            calculator.FirstNumber = number;
        }

        [Given("I have also entered (.*) into the calculator")]
        public void GivenIHaveAlsoEnteredSomethingIntoTheCalculator(int number)
        {
            calculator.SecondNumber = number;
        }

        [When("I press add")]
        public void WhenIPressAdd()
        {
            Result = calculator.Add();
        }

        [When(@"I press multiply")]
        public void WhenIPressMultiply()
        {
            Result = calculator.Multiply();
        }

        [Then("the result should be (.*) on the screen")]
        public void ThenTheResultShouldBe(int testResult)
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(Result, testResult);
        }
    }
}
