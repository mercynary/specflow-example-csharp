﻿Feature: CalculatorMultiply
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the multiple of two numbers

Scenario: Multiply two positive numbers
	Given I have entered 2 into the calculator
	And I have also entered 3 into the calculator
	When I press multiply
	Then the result should be 6 on the screen

Scenario: Multiply two negative numbers
	Given I have entered -2 into the calculator
	And I have also entered -3 into the calculator
	When I press multiply
	Then the result should be 6 on the screen